#!/usr/bin/python3

import pdb
import sys
import os
import time
import glob

import controller
import user_class

print("starting derDurchschlag")


pathToConfig = os.getcwd() + "/config"          #get current directory of application and define path to config file

pathToUsers = ""
pathToInbox = ""
delay = -1
admin = ""
ownNumber  = ""

treshholdToInformAdmin = ""
timeBetweenSelfTests = ""
senderGetsHisOwnMessage = False
#pullMessageKeyword = ""
#pullMessageContent = ""
keywordArray = []                               #this is an array which will contain other arrays. each of them 
defaultAnswer = "There is no default answer"    #this answer will be sent to any non-valid pull-message request. the string will be overwritten while parsing the config-file, if a defaultAnswer is provided      
answerArray = []  
partyMode = False                               #this array will contain the answers. the the answer on the position in this array corresponds with the keyword (aka question) in the keywordArray.
sendTimeStamp = False
helpText = "empty help text"

Debug_lineCounter = 0

try:
    try:
        with open(pathToConfig) as config:
            lines = config.readlines()
    except:
        print("prblem while opening config-file")

    #parsing config-files
    
    for line in lines:
        ###debug
        #Debug_lineCounter += 1
        #if Debug_lineCounter == 6:
        #    pdb.set_trace()
        ###
        
        line = line.split("#")[0]
        line=line.lstrip(" ")
        
        if line.split(" ")[0] == "inbox:":
            pathToInbox = line.split(" ")[1][:-1]
        elif line.split(" ")[0] == "helpText:":
            #pdb.set_trace()
            helpText = ( line.split(" ", 1)[-1] )[:-1]       #the first -1 is to avoid index out of range-errors. and the second, to remove the \n
            #pdb.set_trace()
            if len(helpText) > 1400:
                print("help-text is longer then 1400 symbols. that is not allowed, because it will probably not work. cut it down!")
                exit(1)
        elif line.split(" ")[0] == "users:":
            pathToUsers = line.split(" ")[1][:-1]
        elif line.split(" ")[0] == "delay:":
            delay = int( line.split(" ")[1] )
        elif line.split(" ")[0] == "sendTimeStamp:":
            if line.split(" ")[1] == "False" or line.split(" ")[1][:-1] == "False":
                sendTimeStamp = False
            elif line.split(" ")[1] == "True" or line.split(" ")[1][:-1] == "True":
                sendTimeStamp = True
            else: 
                print("something went wrong parsing 'sendTimeStamp'-parameter in config-file. exit now!") 
                exit(1)
        elif line.split(" ")[0] == "admin:":
            admin = line.split(" ")[1][:-1]
        elif line.split(" ")[0] == "senderGetsHisOwnMessage:":
            value = line.split(" ")[1][:-1]
            if value == "False":
                senderGetsHisOwnMessage = False
            elif value == "True":
                senderGetsHisOwnMessage = True
            else:
                print("error while reading 'senderGetsHisOwnMessage'-value from config file")
                exit(1)
        elif "keyword" in line:
            #pdb.set_trace()
            positionOfKeyword = int( ( line.split(" ")[0][:-1] ).split("keyword")[1] )
            if len(keywordArray) != positionOfKeyword:
                print("something went wrong. maybe the order of keywords in config file is not right. it must be right numerical order starting with keyword0, followed by keyword1...")
                exit(1)
            else:   #is in right order
                #pdb.set_trace()
                line = line.split("#")[0]   #if there is a comment at the end of the line, we remove that...
                line = line.strip()                #and we remove the whitespace
                line = line.split(":")[1]   #then we remove the beginning of the line, which is just 'keywordX'
                line = line.strip()                #and again, removing whitespace
                #pdb.set_trace()
                keywordArray.append( ( line.lower() ).split(",") )
                for index in range(0, len(keywordArray[positionOfKeyword])):
                    keywordArray[positionOfKeyword][index] = keywordArray[positionOfKeyword][index].strip()
                    keywordArray[positionOfKeyword][index] = keywordArray[positionOfKeyword][index].replace("\n", "") #removes the \n at the end of the last keyword in each line of config-file
                #pdb.set_trace()
                pass
        elif "answer" in line:
            #pdb.set_trace()
            positionOfAnswer = int( ( line.split(" ")[0][:-1] ).split("answer")[1] )
            if len(answerArray) != positionOfAnswer:
                print("something went wrong. maybe the order of answers in config file is not right. it must be right numerical order starting with answer0")
                exit(1)
            else:
                line = line.replace("\n", "")
                answerArray.append( line.split(" ",1)[1] )
                #pdb.set_trace()
                pass
        elif line.split(" ")[0][:-1] == "partyMode":
            if line.split(" ")[1] == "True":
                partyMode = True
            elif line.split(" ")[1] == "False":
                partyMode = False
            else:
                print("something went wrong parsing 'partyMode'-parameter in config-file. exit now!") 
                exit(1)
        elif line.split(" ")[0][:-1] == "defaultAnswer":
            line = line.replace("\n", "")
            defaultAnswer = line.split(" ",1)[1]
            #pdb.set_trace()
            pass
        elif line.split(" ")[0][:-1] == "ownNumber":
            if line.split(" ")[1][-1:] == '\n':
                ownNumber = line.split(" ")[1][:-1]
            else:
                ownNumber = line.split(" ")[1]
        elif line.split(" ")[0][:-1] == "timeBetweenSelfTests":
            #pdb.set_trace()
            if line.split(" ")[1][-1:] == '\n':
                timeBetweenSelfTests = int( line.split(" ")[1][:-1] )
            else:
                timeBetweenSelfTests = int( line.split(" ")[1] )
            #pdb.set_trace()
            
        elif line.split(" ")[0][:-1] == "treshholdToInformAdmin":
            if line.split(" ")[1][-1:] == '\n':
                treshholdToInformAdmin = int( line.split(" ")[1][:-1] )
            else:
                treshholdToInformAdmin = int( line.split(" ")[1] )
        else:
            if len(line) != 0:
                #pdb.set_trace()
                if line[0] != "#" and line != "\n":
                    #pdb.set_trace()
                    print("something went wrong parsing config file. line: " + str(line))
                    exit(1)
                else:
                    pass
                    #print("comment found: " + str(line))
            else:
                pass
    #pdb.set_trace()
    pass

    if len(keywordArray) != len(answerArray):
        print("something wrent wrong. len(keywordArray) not len(answerArray)")
        exit(1)
                
except Exception as e:
    print("problem while parsing config-file. Exception:")
    print("stoped at config-file, line: " + str(Debug_lineCounter))
    print(e)
    exit(1)





try:
    controller = controller.Controller(pathToUsers, pathToInbox, delay, senderGetsHisOwnMessage, keywordArray, answerArray, defaultAnswer, partyMode, sendTimeStamp, helpText, ownNumber, timeBetweenSelfTests, treshholdToInformAdmin, admin )
except KeyboardInterrupt as e:
    print("\nshutdown by user")
    print(e)
except Exception as e:
    print("\nentering except: ")
    #pdb.set_trace()
    text = "derDurchschlag is shutting down. bad coding?"
    toSendString =  "echo '" + text + "' | gammu-smsd-inject TEXT " + admin
    os.system( toSendString )
    print("sending SMS to " + admin + " (admin): " + str( toSendString ) )
    print(e)
    time.sleep(20)

exit(1)

